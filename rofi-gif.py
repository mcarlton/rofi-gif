#!/usr/bin/python

import sys
from subprocess import Popen, PIPE
import yaml
import os

"""
`dir_path` is only needed if your tag file is in the
same directory as rofi-gif.py.  Otherwise, set `configFile
equal to the full path to your tag file
"""
dir_path = os.path.dirname(os.path.realpath(__file__)) + '/'
configFile = dir_path + 'example.yaml'

def get_data():
    """
    Read the config file, load the values into
    a list of dictionaries
    """
    listChoices = []
    with open(configFile, 'r') as file:
        options = yaml.load(file)
    for entry,data in options.items():
        """
        Note: this is ugly.  Really ugly.
        I can't figure out how to make text invisible but still searchable.
        The solution is to just make each entry super long with plenty of
        whitespace between data.  Technically, the tags  are in the rofi box
        and searchable, but the lines are long enough they're rendered outside
        the box and not visible.  If your rofi box is larger or you can see
        your tags appearing in the list, adjust this value to be higher
        """
        numChars=75
        description = data['description'].ljust(numChars, ' ')
        tags = str(data.get('tags', ''))[1:-1].rjust(numChars, ' ')
        listChoices.append({'url': entry, 'choice': description + tags})
    return(listChoices)

data = get_data()

if len(sys.argv) > 1:
    """
    If a selection is chosen, then copy the URL to the system keyboard
    """
    choice = next((item for item in data if item['choice'] == sys.argv[1]))
    Popen(('xsel', '-ib'), stdin=PIPE).communicate(str.encode(choice['url']))
    sys.exit()
else:
    """
    Generate the list of options
    """
   for entry in data:
       print(entry['choice'])
