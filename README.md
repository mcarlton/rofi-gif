# rofi-gif
Maintain your own local gif database and access it using [rofi](https://github.com/DaveDavenport/rofi).

## Overview

Born from frustration between services not having a built in gif searcher, or the gif search functionality not always giving good results, rofi-gif will integrate into the `rofi` launcher and give you access to a gif library curated to exactly what you want.

## Example

Full list of entries from file

![alt text](screenshots/full.png "Full Results")

Searching for a tag

![alt text](screenshots/search.png "Search Results")

## Requirements

* PyYAML
* XSel

## Usage

Create a tag file in the following format:
```yaml
URL_TO_GIF:
    description: A description of the gif you'll understand
    tags: (note - optional)
      - more
      - things
      - to
      - match
      - against
```
Edit line 14 of `rofi-gif.py` to have the location of your newly created tag file.

Add `gif:/path/to/rofi-gif.py` to your rofi configuration file.

Example config:
```
rofi.modi:    window,run,gif:/home/mcarlton/bin/rofi-gif.py
```

After searching for and selecting a gif, it will be copied into your system clipboard using the `xsel` command.
